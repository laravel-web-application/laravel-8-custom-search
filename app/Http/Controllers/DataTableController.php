<?php

namespace App\Http\Controllers;

use App\Models\User;

class DataTableController extends Controller
{
    public function index()
    {
        if (request()->ajax()) {

            $query = User::query();

            $from_date = (!empty($_GET["from_date"])) ? ($_GET["from_date"]) : ('');
            $to_date = (!empty($_GET["to_date"])) ? ($_GET["to_date"]) : ('');

            if ($from_date && $to_date) {

                $from_date = date('Y-m-d', strtotime($from_date));
                $to_date = date('Y-m-d', strtotime($to_date));

                $query->whereRaw("date(users.created_at) >= '" . $from_date . "' AND date(users.created_at) <= '" . $to_date . "'");
            }
            $users = $query->select('*');
            return datatables()->of($users)
                ->make(true);
        }
        return view('list');
    }
}
