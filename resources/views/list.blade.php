<!DOCTYPE html>

<html lang="en">
<head>

    <title>Laravel DataTables Example</title>

    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>

    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">

    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">

    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('/js/moment/moment.min.js') }}"></script>
    <script src="{{ asset('/js/moment/locales.js') }}"></script>


</head>
<body>
<div class="container mt-4">
    <h2 class="text-center mt-2 mb-3 alert alert-success">Laravel DataTable Example</h2>
    <div class="row">

        <div class="form-group col-md-5">
            <h5>From Date <span class="text-danger"></span></h5>
            <div class="controls">
                <input type="date" name="from_date" id="from_date" class="form-control datepicker-autoclose"
                       placeholder="Please from date">
            </div>
        </div>

        <div class="form-group col-md-5">
            <h5>To Date <span class="text-danger"></span></h5>
            <div class="controls">
                <input type="date" name="to_date" id="to_date" class="form-control datepicker-autoclose"
                       placeholder="Please to date">
            </div>
        </div>

        <div class="form-group col-md-2" style="margin-top: 32px;">
            <div class="controls">
                <button type="text" id="btn-search" class="btn btn-info form-control">Submit</button>
            </div>
        </div>

    </div>
    <table class="table table-bordered" id="laravel-datatable">
        <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Email</th>
            <th>Created at</th>
        </tr>
        </thead>
    </table>
</div>
<script>
    $(document).ready(function () {
        $('#laravel-datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ url('list') }}",
                type: 'GET',
                data: function (d) {
                    d.from_date = $('#from_date').val();
                    d.to_date = $('#to_date').val();
                }
            },
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {
                    data: 'created_at',
                    name: 'created_at',
                    render: function (data, type, row) {
                        return moment(data).locale('id').format('LL');
                    }
                }
            ]
        });
    });
    $('#btn-search').click(function () {
        $('#laravel-datatable').DataTable().draw(true);
    });
</script>
</body>
</html>
